# Chabot-io  
This project is a pure HTML/JS/CSS website that mimics a working chatbot application.  
You can access available commands for each bot by typing either `help` or `/help`.  
Alternatively, typing this command in the group chat will print the available commands for all bots.

## Launching the app  
Double-click the `index.html` file. It's as simple as that.

## ChatGPT API
In order to use the `gpt` command, you need to input your OpenAI API key. The corresponding line can be found on line 344 of the `js/app.js` file.