// Luxon reference we'll use to manage dates across the app
const DateTime = luxon.DateTime;

/**
 * As soon as the document is ready, we add a listener on the message input bar that will send the input whenever Enter is pressed.
 * We also scroll down to the bottom of the message box, just in case we have more messages than the screen can display.
 */
$(document).ready(function(){
    $('#message-input').keypress(function(e){
        if(e.keyCode==13)
            sendMessage();
    });
    $('#messages').scrollTop($('#messages')[0].scrollHeight);
});

// This stores the current room. It *should* be an ID, but it's the bot/room name for now.
let currentRoom = 'Manu';
/**
 * All the available bots are stored in this array:
 * - Name defines the display name of the bot. It is also its unique identifier across the functions in this app.
 * - Avatar is a URL to a profile picture to display in the chat.
 * - Messages is internal only, and is used to store the messages exchanged with this bot.
 * - Commands is an array of objects containing the following properties:
 * -- Name: the name of the command, used for the help command.
 * -- Description: the description of the command, used for the help command.
 * -- Fn: an anonymous function that contains the action that will be executed when this command is called. Also passes the current bot object, and the args of the command.
 * -- Triggers: an array of strings containing the chat triggers that need to be input for this command to be executed.
 **/
let bots = [
    {
        name: 'Manu',
        avatar: 'https://upload.wikimedia.org/wikipedia/commons/d/d2/Emmanuel_Macron_June_2022_%28cropped%29.jpg',
        messages: [],
        commands: [
            {
                name: 'Time',
                description: 'Displays the current date and time.',
                fn: (bot, args) => reply(timeCommand(), bot),
                triggers: ['/time']
            },
            {
                name: 'Weather',
                description: 'Gives you the weather at the provided location.',
                fn: (bot, args) => reply(weatherCommand(args), bot),
                triggers: ['/weather']
            },
            {
                name: 'Hello',
                description: 'Common command across all bots. Self introduction.',
                fn: (bot, args) => reply(helloCommand('manu'), bot),
                triggers: ['Hello!', 'Hello', '/hello']
            },
            {
                name: 'Help',
                description: 'Displays available commands.',
                fn: (bot, args) => reply(helpCommand('Manu'), bot),
                triggers: ['Help!', 'Help', '/help']
            },
            {
                name: 'ChatGPT',
                description: 'Sends a request to ChatGPT.',
                fn: (bot, args) => reply(gptCommand(args), bot),
                triggers: ['ChatGPT', '/chatgpt', '/gpt']
            }
        ]
    },
    {
        name: 'Eric',
        messages: [],
        avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Portrait_d%27%C3%89ric_Zemmour%2C_avril_2022.jpg/640px-Portrait_d%27%C3%89ric_Zemmour%2C_avril_2022.jpg',
        commands: [
            {
                name: 'Kick',
                description: 'Kicks immigr-- another bot from the channel. Be careful with it.',
                fn: (bot, args) => reply(kickCommand(args), bot),
                triggers: ['Kick', '/kick']
            },
            {
                name: 'Quote',
                description: 'Outputs a random quote.',
                fn: (bot, args) => reply(quoteCommand(), bot),
                triggers: ['/quote']
            },
            {
                name: 'Hello',
                description: 'Common command across all bots. Self introduction.',
                fn: (bot, args) => reply(helloCommand('eric'), bot),
                triggers: ['Hello!', 'Hello', '/hello']
            },
            {
                name: 'Help',
                description: 'Displays available commands.',
                fn: (bot, args) => reply(helpCommand('Eric'), bot),
                triggers: ['Help!', 'Help', '/help']
            },
            {
                name: 'ChatGPT',
                description: 'Sends a request to ChatGPT.',
                fn: (bot, args) => reply(gptCommand(args), bot),
                triggers: ['ChatGPT', '/chatgpt', '/gpt']
            }
        ]
    },
    {
        name: 'Marine',
        messages: [],
        avatar: 'https://upload.wikimedia.org/wikipedia/commons/e/e1/Le_Pen%2C_Marine-9586_%28cropped%29.jpg',
        commands: [
            {
                name: 'Jeanne',
                description: 'Invocates the almighty Jean-Marie.',
                fn: (bot, args) => reply(jeanneCommand(), bot),
                triggers: ['Oskour!', '/jeanne']
            },
            {
                name: 'Fuck',
                description: 'Tells you to fuck off gracefully.',
                fn: (bot, args) => reply(fuckCommand(args), bot),
                triggers: ['Fuck!', '/fuckoff']
            },
            {
                name: 'Hello',
                description: 'Common command across all bots. Self introduction.',
                fn: (bot, args) => reply(helloCommand('marine'), bot),
                triggers: ['Hello!', 'Hello', '/hello']
            },
            {
                name: 'Help',
                description: 'Displays available commands.',
                fn: (bot, args) => reply(helpCommand('Marine'), bot),
                triggers: ['Help!', 'Help', '/help']
            },
            {
                name: 'ChatGPT',
                description: 'Sends a request to ChatGPT.',
                fn: (bot, args) => reply(gptCommand(args), bot),
                triggers: ['ChatGPT', '/chatgpt', '/gpt']
            }
        ]
    },
    {
        name: 'Mélou',
        messages: [],
        avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Meeting_M%C3%A9lenchon_Toulouse_-_2017-04-16_-_Jean-Luc_M%C3%A9lenchon_-_41_%28cropped_2%29.jpg/220px-Meeting_M%C3%A9lenchon_Toulouse_-_2017-04-16_-_Jean-Luc_M%C3%A9lenchon_-_41_%28cropped_2%29.jpg',
        commands: [
            {
                name: 'Spawn',
                description: 'Spawns immigrants.',
                fn: (bot, args) => reply(spawnCommand(), bot),
                triggers: ['Immigrants.', '/spawn']
            },
            {
                name: 'Hello',
                description: 'Common command across all bots. Self introduction.',
                fn: (bot, args) => reply(helloCommand('mélou'), bot),
                triggers: ['Hello!', 'Hello', '/hello']
            },
            {
                name: 'Help',
                description: 'Displays available commands.',
                fn: (bot, args) => reply(helpCommand('Mélou'), bot),
                triggers: ['Help!', 'Help', '/help']
            },
            {
                name: 'ChatGPT',
                description: 'Sends a request to ChatGPT.',
                fn: (bot, args) => reply(gptCommand(args), bot),
                triggers: ['ChatGPT', '/chatgpt', '/gpt']
            }
        ]
    },
];

/**
 * Rooms pretty much have the same function as bots, minus the commands which are for each individual bot.
 * You can set which members are in this room by editing the members array and inputting the bot UID/name.
 */
let rooms = [
    {
        name: '49.3 party',
        members: ['Manu', 'Eric', 'Marine', 'Mélou'],
        avatar: 'https://img.freepik.com/free-vector/silhouette-party-audience_1048-9714.jpg?w=2000',
        messages: []
    }
]

/**
 * We call the init function when the page is loaded.
 */
onInit();

/**
 * This function executes a series of actions depending on what command and bot are passed.
 * @param command String containing the command that was input by the user.
 * @param bot Bot object that this message was sent to.
 */
function reply(command, bot) {
    displayMessage(generateMessage(command, new Date(), bot));
    saveMessage(command, false);
    $('#messages').scrollTop($('#messages')[0].scrollHeight);
}

/**
 * Help command that displays the available commands for the provided bot name.
 * @param name Name/UID of the bot whose commands need to be explained.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function helpCommand(name) {
    const bot = findBot(name)
    let str = `Here are my available commands:<br><br>`;
    bot.commands.forEach((command) => {
        str += `${command.description} - Triggered by: ${command.triggers.join(', ')}<br>`;
    });
    return str;
}

/**
 * Hello command that is common across all bots.
 * @param bot Bot identifier that defines what the reply will be.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function helloCommand(bot) {
    switch (bot) {
        case 'manu':
            return "C'EST NOTRE PROJET !";
        case 'eric':
            return "Je ne parle pas aux étrangers.";
        case 'marine':
            return "Abonnez vous à la chaîne de mon père, JMLP Gaming.";
        case 'mélou':
            return "Je me présente, je suis la démocratie.";
    }
}

/**
 * Spanw command that creates three new bots and hot-loads them.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function spawnCommand() {
    bots.push(
        {
            name: 'Mohammed',
            messages: [],
            avatar: 'https://media.istockphoto.com/id/1173663575/photo/portrait-of-a-confident-arab-businessman-wearing-uae-emirati-traditional-dress.jpg?b=1&s=170667a&w=0&k=20&c=29JZLYpFnHdp3tk1txHMEA5RK6MX8ReAKLIZNrbmDyI=',
            commands: []
        },
        {
            name: 'Saad',
            messages: [],
            avatar: 'https://images.unsplash.com/photo-1577475766758-21071075ff92?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8YXJhYiUyMG1hbnxlbnwwfHwwfHw%3D&w=1000&q=80',
            commands: []
        },
        {
            name: 'Omar',
            messages: [],
            avatar: 'https://thumbs.dreamstime.com/b/attractive-arabic-man-smiling-copy-space-attractive-arabic-man-smiling-copy-space-157615737.jpg',
            commands: []
        },
    );
    refreshBotList();
    return 'Mohammed, Saad and Omar have joined the channel.';
}

/**
 * Time command that displays the current time in the chat.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function timeCommand() {
    return 'It is currently ' + DateTime.now().toFormat('HH:mm') + ' on ' + DateTime.now().toFormat('DDD') + '.';
}

/**
 * Weather command that queries the OWM API to get weather data in the provided location.
 * @param args Array of arguments passed after the command. Only the first one will be used.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function weatherCommand(args) {
    if (!args[0]) {
        return 'Please input a city name to fetch its current weather.';
    }
    let weather;
    $.ajax({
        url: "https://api.openweathermap.org/data/2.5/weather?q=" + args[0] + "&appid=492161504be95dedc724851e8c669702&units=metric&lang=fr",
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            weather = data;
        }
    });
    return 'It is currently ' + weather.main.temp + ' degrees Celsius in ' + args[0] + '.';
}

/**
 * Quote command that calls the Quotable API to get a random quote and output it in the chat.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function quoteCommand() {
    let quote;
    $.ajax({
        url: "https://api.quotable.io/random",
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            quote = data;
        }
    });
    return quote.content;
}

/**
 * Fuck off command that calls the FOAAS API and returns a very nice message, signed by whatever name is after the command.
 * @param args Array of arguments passed after the command. Only the first one will be used.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function fuckCommand(args) {
    if (!args[0]) {
        return 'Please input a name to sign the message with.';
    }
    let fuckOff;
    $.ajax({
        url: "https://foaas.com/asshole/" + args[0],
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            fuckOff = data;
        }
    });
    return fuckOff.message + " " + fuckOff.subtitle;
}

/**
 * GPT command that calls the ChatGPT API to display an answer using GPT 3.5. Kind of a proxy for ChatGPT. Your query should be passed after the command.
 * Note that your API key needs to be set on line 344.
 * @param args Array of arguments passed after the command. Only the first one will be used.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function gptCommand(args) {
    if (!args[0]) {
        return 'Please input a prompt to send to ChatGPT.';
    }
    let apiKey = "";
    if (apiKey === '') {
        return 'Please input your OpenAPI key in the app.js file to use this command.';
    }
    let reply;
    $.ajax({
        url: "https://api.openai.com/v1/chat/completions",
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            "model": "gpt-3.5-turbo",
            "messages": [{"role": "user", "content": args.join(' ')}]
        }),
        headers: {"Authorization": "Bearer " + apiKey},
        async: false,
        success: function(data) {
            reply = data;
        }
    });
    return reply.choices[0].message.content;
}

/**
 * Kick command that removes the selected bot from the app. The name/UID of the bot needs to be passed after the command.
 * @param args Array of arguments passed after the command. Only the first one will be used.
 * @returns {string|string} Formatted HTML string that will be displayed in the chat.
 */
function kickCommand(args) {
    if (!args[0]) {
        return 'Please give me an immigrant to kick out of this channel.';
    }
    let deleted = false;
    bots.forEach((bot) => {
        if (bot.name.toLowerCase() === args[0].toLowerCase()) {
            bots.splice(bots.indexOf(bot), 1);
            refreshBotList();
            deleted = true;
        }
    });
    return deleted ? 'I kicked ' + args[0] + ' out of the channel. Go back to your country.' : "I couldn't find the immigrant you targeted.";
}

/**
 * Jeanne command that... pretty much does nothing.
 * @returns {string} Formatted HTML string that will be displayed in the chat.
 */
function jeanneCommand() {
    return 'JEANNE, AU SECOURS !';
}

/**
 * Init command called when the app loads. It loads the cached messages from the local storage, opens the default chat room, and adds the bots to the side list.
 */
function onInit() {
    loadStorage();
    openChatRoom(currentRoom);
    refreshBotList();
}

/**
 * Cache retrieval function that gets the messages for both rooms and bots and assigns them to the correct objects to get back to where you stopped last time.
 */
function loadStorage() {
    if (localStorage.getItem("CHATBOT_IO_CACHE_MSG")) {
        JSON.parse(localStorage.getItem("CHATBOT_IO_CACHE_MSG")).forEach(cache => {
            if (!findBot(cache.name)) {
                return;
            }
            findBot(cache.name).messages = cache.messages;
        });
    }
    if (localStorage.getItem("CHATBOT_IO_CACHE_ROOMS")) {
        JSON.parse(localStorage.getItem("CHATBOT_IO_CACHE_ROOMS")).forEach(cache => {
            findRoom(cache.name).messages = cache.messages;
        });
    }
}

/**
 * Cache storage function that loops on all bots and rooms and saves their messages in the local storage for later retrieval.
 */
function saveStorage() {
    const msg = [];
    bots.forEach(bot => {
        msg.push({
            name: bot.name,
            messages: bot.messages
        });
    });
    localStorage.setItem("CHATBOT_IO_CACHE_MSG", JSON.stringify(msg));
    const rm = [];
    rooms.forEach(room => {
        rm.push({
            name: room.name,
            messages: room.messages
        });
    });
    localStorage.setItem("CHATBOT_IO_CACHE_ROOMS", JSON.stringify(rm));
}

/**
 * Function that refreshes the side list, in case new bots were added/removed.
 */
function refreshBotList() {
    $("#contacts").html(`<span class="text-xl font-bold">Contacts</span>
                <div class="w-full border-b border-white mt-2 mb-2"></div>`);
    rooms.forEach((room) => {
        $("#contacts").append(generateRoomCard(room));
    });
    bots.forEach((bot) => {
        $("#contacts").append(generateContactCard(bot));
    });
}


/**
 * Function that parses the user input to get the arguments and the command to execute.
 * @param input User input string.
 */
function parseInput(input) {
    if (findBot(currentRoom)) {
        const bot = findBot(currentRoom);
        parseCommand(input, bot);
    } else {
        const room = findRoom(currentRoom);
        room.members.forEach((member) => {
            const bot = findBot(member);
            parseCommand(input, bot);
        })
    }
}

/**
 * Function that tries to find the provided command in the provided bot. If found, the command's function will be executed. If not, nothing will happen.
 * @param input Command string.
 * @param bot Bot object whose commands should be checked.
 */
function parseCommand(input, bot) {
    const parts = input.split(' ');
    bot.commands.forEach((command) => {
        command.triggers.forEach((trigger) => {
            if (parts[0].toLowerCase() === trigger.toLowerCase()) {
                const args = parts.splice(1, parts.length);
                command.fn(bot, args);
            }
        })
    })
}

/**
 * Command that returns a bot that has the provided name.
 * @param name Name of the bot that is being searched for.
 */
function findBot(name) {
    for (let i = 0; i < bots.length; i++) {
        if (bots[i].name === name) {
            return bots[i];
        }
    }
}

/**
 * Command that returns a room that has the provided name.
 * @param name Name of the room that is being searched for.
 */
function findRoom(name) {
    for (let i = 0; i < rooms.length; i++) {
        if (rooms[i].name === name) {
            return rooms[i];
        }
    }
}

/**
 * Function that processes the user input (caching, command resolution, message display...)
 */
function sendMessage() {
    const msg = $('#message-input').val();
    saveMessage(msg, true);
    $('#message-input').val('');
    displayMessage(generateMessage(msg, new Date()));
    parseInput(msg);
}

/**
 * Function that displays the provided message node in the chat area.
 * @param messageNode HTML string that should be displayed.
 */
function displayMessage(messageNode) {
    $("#messages").append(messageNode);
}

/**
 * Function called when the user clicks a bot. It changes the currently loaded bot and properly refreshes the message area.
 * @param botName
 */
function openChatRoom(botName) {
    const bot = findBot(botName);
    currentRoom = bot.name;
    $('#dest').html(currentRoom);
    loadMessages(bot);
}

/**
 * Function called when the user clicks a room. It changes the currently loaded room and properly refreshes the message area.
 * @param roomName
 */
function openGroupChat(roomName) {
    const room = findRoom(roomName);
    currentRoom = room.name;
    $('#dest').html(currentRoom);
    loadMessages(room);
}

/**
 * Function that displays the messages of the selected channel (bot or room) in the message area.
 * @param channel Room/bot whose messages should be loaded on screen.
 */
function loadMessages(channel) {
    $('#messages').html('');
    channel.messages.forEach((message) => {
        displayMessage(generateMessage(message.message, message.date, message.user ? null : channel));
    });
}

/**
 * Function that generates an HTML node to display on the left area, symbolizing a chat room.
 * @param room Room object whose card should be generated.
 * @returns {string} HTML node to display.
 */
function generateRoomCard(room) {
    return `
    <div class="w-full h-16 flex items-center p-4 cursor-pointer" onclick="openGroupChat('${room.name}')">
        <img src="${room.avatar}" class="avatar">
        <div class="flex flex-col ml-4">
            <span class="text-lg">${room.name}</span>
            <div class="flex items-center">
                <span class="text-sm text-gray-300">Group chat</span>
            </div>
        </div>
    </div>
    `;
}

/**
 * Function that generates an HTML node to display on the left area, symbolizing a bot discussion.
 * @param bot Bot object whose card should be generated.
 * @returns {string} HTML node to display.
 */
function generateContactCard(bot) {
    return `
    <div class="w-full h-16 flex items-center p-4 cursor-pointer" onclick="openChatRoom('${bot.name}')">
        <img src="${bot.avatar}" class="avatar">
        <div class="flex flex-col ml-4">
            <span class="text-lg">${bot.name}</span>
            <div class="flex items-center">
                <div class="h-2 w-2 rounded-full bg-green-500"></div>
                <span class="ml-2 text-sm text-gray-300">Online</span>
            </div>
        </div>
    </div>
    `;
}

/**
 * Function that saves the provided message in the currently loaded bot's temporary cache.
 * Not to be confused with the cache above, which stores in local storage. This is session-only, used to quick-load when swapping rooms.
 * @param message Message that should be stored.
 * @param user Whether this message was sent by a bot or the user.
 */
function saveMessage(message, user) {
    if (findBot(currentRoom)) {
        findBot(currentRoom).messages.push({
            message,
            date: new Date(),
            user
        });
    } else {
        findRoom(currentRoom).messages.push({
            message,
            date: new Date(),
            user
        });
    }
    saveStorage();
}

/**
 * Function that generates the HTML node for a message.
 * @param message Message that should be displayed in the bubble.
 * @param time Time at which the message was sent.
 * @param bot Bot object that the discussion is taking place with. If this is null, it is assumed the user sent the message.
 * @returns {string} HTML node.
 */
function generateMessage(message, time, bot = null) {
    time = new Date(time);
    if (!bot) {
        return `
        <div class="w-full flex flex-row-reverse ml-auto mb-2">
            <img class="avatar-sm ml-4 mt-1" src="https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png?20150327203541">
            <div class="flex flex-col justify-end">
                <div class="max-w-xl bg-slate-500 rounded pl-3 pr-3 pt-2 pb-2 break-all w-fit ml-auto">
                    ${message}
                </div>
                <span class="text-gray-400 text-xs mt-1">Me, ${DateTime.fromJSDate(time).toLocaleString(DateTime.TIME_WITH_SECONDS)}</span>
            </div>
        </div>
        `;
    }
    return `
        <div class="w-full flex flex-row mb-2">
            <img class="avatar-sm mr-4 mt-1" src="${bot.avatar}">
            <div class="flex flex-col">
                <div class="max-w-xl bg-slate-500 rounded pl-3 pr-3 pt-2 pb-2 break-all w-fit">
                    ${message}
                </div>
                <span class="text-gray-400 text-xs mt-1">${bot.name}, ${DateTime.fromJSDate(time).toLocaleString(DateTime.TIME_WITH_SECONDS)}</span>
            </div>
        </div>
    `;
}